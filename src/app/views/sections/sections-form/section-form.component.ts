import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Section } from '../../../core/models/section';
import { SectionService } from '../../../core/services/sections.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { QuestionService } from '../../../core/services/questions.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-section-form',
    templateUrl: './section-form.component.html',
    styleUrls: ['./section-form.component.scss'],
})
export class SectionFomrComponent implements OnInit {
    @Input('initialValues') initialValuesProps: any;
    @Output('questionSubmit') sectionEvent = new EventEmitter<Section>();

    defaul_etat = 'Activée';
    form: FormGroup;

    questions = [];
    sections_questions = [];
    sectionsList = [];
    constructor(
        private fb: FormBuilder,
        private sectionService: SectionService,
        private questionService: QuestionService
    ) {}
    ngOnInit(): void {
        this.initializeValues();
        this.sectionsList = this.sectionService.getSections();
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(
                event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex
            );
        }
        if (!this.sections_questions.includes(event.container.data[event.currentIndex])) {
            this.sections_questions.push(event.container.data[event.currentIndex]);
            if (event.previousContainer.id === 'cdk-drop-list-0' && event.container.id === 'cdk-drop-list-1') {
                this.affectQuestionToSection(this.form.value, event.container.data[event.currentIndex]);
            }
        }
    }
    affectQuestionToSection(section: Section, question: any) {
        console.log(section);
        this.sectionService.affecterQuestion(section.id, question.id);
        this.sections_questions.push(question);
    }

    getQuestionDataById(questionId: number) {
        const question = this.questionService.getQuestions().find((el) => el.id === questionId);
        return { id: question.id, name: question.name, type: question.type };
    }

    private initializeValues() {
        this.questions = this.questionService.getQuestions();
        this.form = this.fb.group({
            etat: new FormControl(
                this.initialValuesProps.etat != null ? this.initialValuesProps.etat : this.defaul_etat
            ),
            titre: new FormControl(this.initialValuesProps?.titre, {
                validators: [Validators.required, Validators.maxLength(60)],
            }),
            type: new FormControl('', Validators.required),
            questions: new FormControl(this.initialValuesProps.questions || []),
            id: this.initialValuesProps?.id,
        });
        if (this.initialValuesProps?.id) {
            const section = this.sectionService.getSectionById(this.initialValuesProps.id);
            if (section) {
                this.sections_questions = section.questions || [];
            }
        }
    }

    save() {
        this.questions = this.questionService.getQuestions();
        this.sectionEvent.emit(this.form.value);
    }
}
