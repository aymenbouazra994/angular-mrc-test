import { Injectable } from '@angular/core';
import { question_section, sections } from './data';
import { Section } from '../models/section';

@Injectable({
    providedIn: 'root',
})
export class SectionService {
    temp_sections = [...sections];
    temp_question_section = [...question_section];

    constructor() {}

    createSection(section: Section) {
        let id = Math.floor(Math.random() * 100000000) + 1;

        let temp_section = {
            id: id,
            titre: section.titre,
            etat: section.etat,
            questions:section.questions
        };
        this.temp_sections.push({ ...temp_section });
    }
    updateSection(section: Section) {
        var index = this.temp_sections.findIndex((x) => x.id == section.id);

        this.temp_sections[index] = section;        
    }
    deleteSection(section: Section) {
        this.temp_sections = this.temp_sections.filter((el)=> el.id !== section.id)
    }

    affecterQuestion(section_id,question_id) {
        const section = this.temp_sections.find((el) => el.id === section_id);
        console.log(section);
        
        if (section) {
            const questionIndex = this.temp_question_section.findIndex((el)=> el === question_id)
            
            if (questionIndex === -1) {
                let id = Math.floor(Math.random() * 100000000) + 1;
                this.temp_question_section.push({id:id,sectionId:section_id,questionId:question_id});
            }
            
            console.log(questionIndex);
            console.log(this.temp_question_section);
            
            this.updateSection({...section,questions:this.temp_question_section});
        }
        console.log(section);
        
    }

    getSectionById(sectionId: number): Section | undefined {
        return this.temp_sections.find((section) => section.id === sectionId);
    }

    getSections() {
        return this.temp_sections;
    }
}
